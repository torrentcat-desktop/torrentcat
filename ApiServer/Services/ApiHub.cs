﻿using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiServer.Services
{
    public class ApiHub : Hub
    {
        private readonly ILogger<ApiHub> _logger;
        private List<string> _stations = new List<string>();
        private static object _dictlock = new object();
        private readonly string _guid = "";

        public ApiHub(ILogger<ApiHub> logger)
        {
            _logger = logger;
            var key = Registry.CurrentUser.OpenSubKey(@"SOFTWARE\TorrentCat", true);
            _guid = key.GetValue("GUID").ToString();
        }

        [HubMethodName("Connect")]
        public async Task Connect(string message)
        {
            _logger.LogInformation($"Client connected: {Context.ConnectionId} {message}");
            await Task.Delay(50);
        }

        private Task SendMessageToStation(string connectionID, string message)
        {
            return Clients.Clients(connectionID).SendAsync("Status", message);
        }

        private Task SendMessageToAll(string message)
        {
            return Clients.All.SendAsync("Status", message);
        }

        private Task SendMessageToCaller(string message)
        {
            return Clients.Caller.SendAsync("Status", message);
        }

        private Task SendMessageToGroup(string message)
        {
            return Clients.Group("TorrentCat").SendAsync("Status", message);
        }

        public override async Task OnConnectedAsync()
        {
            await Groups.AddToGroupAsync(Context.ConnectionId, "TorrentCat");
            await base.OnConnectedAsync();
            _logger.LogInformation($"Connected {Context.ConnectionId}");
            await SendMessageToStation(Context.ConnectionId, _guid);
        }

        public override async Task OnDisconnectedAsync(Exception exception)
        {
            await Groups.RemoveFromGroupAsync(Context.ConnectionId, "TorrentCat");
            await base.OnDisconnectedAsync(exception);
            _logger.LogInformation($"{Context.ConnectionId}:{_stations.Count}");
            var item = _stations.FirstOrDefault(x => x == Context.ConnectionId);
            _logger.LogInformation($"Disconnected station {item}");
            lock (_dictlock)
            {
                _stations.Remove(item);
            }
        }
    }
}
