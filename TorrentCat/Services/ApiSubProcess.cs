﻿using System;
using System.Diagnostics;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Windows;

namespace TorrentCat.Services
{
    public class ApiSubProcess : IDisposable
    {
        private readonly string _baseURL;
        private Process _api_pid = null;
        private bool isDisposed;

        public ApiSubProcess()
        {
            _baseURL = "http://localhost:8181";
        }

        public bool Run()
        {
            CheckApi();
            return _api_pid != null;
        }

        public void Stop()
        {
            _api_pid?.Kill();
            _api_pid = null;
        }

        public bool IsRunning()
        {
            return _api_pid != null;
        }

        public async Task<bool> IsApiRunning()
        {
            using (var client = new HttpClient())
            {
                try
                {
                    //client.Timeout = TimeSpan.FromSeconds(15);
                    //client.DefaultRequestHeaders.Authorization =
                    //    new AuthenticationHeaderValue("Basic", Convert.ToBase64String(System.Text.Encoding.ASCII.GetBytes("1user:1pass")));
                    var response = await client.GetAsync($"{_baseURL}/api/downloads");
                    response.EnsureSuccessStatusCode();
                }
                catch (Exception e)
                {
                    Debug.WriteLine(e.Message);
                    return false;
                }
            }
            return true;
        }

        private void CheckApi()
        {
            if (!File.Exists($@"{AppDomain.CurrentDomain.BaseDirectory}ApiServer.exe"))
            {
                Debug.WriteLine("Failed to run API. Not found ApiServer.exe component.");
                return;
            }

            if (!Task.Run(() => IsApiRunning()).Result)
            {
                Debug.WriteLine("API instance not found. Run new API instance.");
                RunApi();
            }

            Debug.WriteLine("API instance is running. Using this instance.");
        }

        private void RunApi()
        {
            var process = new Process();
            var startInfo = new ProcessStartInfo
            {
                FileName = "ApiServer.exe",
                CreateNoWindow = true,
                RedirectStandardOutput = true,
                RedirectStandardError = true,
                UseShellExecute = false
            };

            _api_pid = process;
            process.StartInfo = startInfo;
            process.EnableRaisingEvents = true;
            
            // Handle program output
            process.OutputDataReceived += new DataReceivedEventHandler((s, o) =>
            {
                if (!string.IsNullOrWhiteSpace(o.Data))
                    Debug.WriteLine(o.Data);

            });
            process.ErrorDataReceived += new DataReceivedEventHandler((s, o) =>
            {
                if (!string.IsNullOrWhiteSpace(o.Data))
                    Debug.WriteLine(o.Data);
            });
            
            // Handle end of execution
            process.Exited += new EventHandler((s, e) =>
            {
                _api_pid = null;
                Debug.WriteLine($"API process termnated");
            });

            try
            {
                process.Start();
                process.BeginOutputReadLine();
                process.BeginErrorReadLine();
            }
            catch (Exception e)
            {
                Debug.WriteLine($"Failed to run API. Error: '{e.Message}'");
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (isDisposed) return;
            if (disposing)
            {
                _api_pid?.Kill();
            }
            isDisposed = true;
        }
    }
}
