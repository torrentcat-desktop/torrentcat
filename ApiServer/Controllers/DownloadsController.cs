﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using ApiServer.Models;
using ApiServer.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace ApiServer.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DownloadsController : ControllerBase
    {
        private readonly ILogger<DownloadsController> _logger;
        private readonly Downloader _downloader;

        public DownloadsController(ILogger<DownloadsController> logger, Downloader downloader)
        {
            _logger = logger;
            _downloader = downloader;
        }

        [HttpGet]
        public IEnumerable<TorrentItem> Index()
        {
            return _downloader.GetTorrents();
        }

        [HttpPut("[action]")]
        public IActionResult Start([FromBody] TorrentItem item)
        {
            _ = Task.Run(() => _downloader.StartDownloadAsync(item)).ConfigureAwait(false);
            return Ok();
        }

        [HttpPost("[action]")]
        [Consumes("multipart/form-data")]
        public async Task<IActionResult> StartFile(IFormCollection formdata)
        {
            var files = HttpContext.Request.Form.Files;
            Dictionary<string, int> items = new Dictionary<string, int>();
            
            foreach (var file in files)
            {
                if (file.Length > 0)
                {
                    var fileName = file.FileName; // Give file name
                    var filePath = $"{Path.GetTempPath().Replace(@"\", "/")}{fileName}";
                    using (var fileStream = new FileStream(filePath, FileMode.Create))
                    {
                        await file.CopyToAsync(fileStream);
                    }

                    var rnd = new Random();
                    var item = new TorrentItem()
                    {
                        ID = rnd.Next(800000000, 900000000),
                        File = $"file://{filePath}",
                        Movie = new MovieItem()
                        {
                            Title = "",
                            Type = "user"
                        }
                    };
                    _ = Task.Run(() => _downloader.StartDownloadAsync(item)).ConfigureAwait(false);
                    items.Add(fileName, item.ID);
                }
            }

            return Ok(items);
        }

        [HttpPut("[action]/{id}")]
        public async Task<IActionResult> Stop(int id)
        {
            var item = await _downloader.StopDownloadAsync(id);
            return Ok(item);
        }

        [HttpDelete("[action]/{id}")]
        public IActionResult Remove(int id)
        {
            _ = Task.Run(() => _downloader.RemoveTorrent(id)).ConfigureAwait(false);
            return Ok();
        }

        [HttpGet("[action]/{id}")]
        public IActionResult Open(int id)
        {
            _ = Task.Run(() => _downloader.OpenFolder(id)).ConfigureAwait(false);
            return Ok();
        }
    }
}
