﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Text;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

using CefSharp;
using CefSharp.WinForms;

using LibVLCSharp.Shared;

using MonoTorrent;
using MonoTorrent.Client;
using MonoTorrent.Client.PiecePicking;
using MonoTorrent.Streaming;

using Newtonsoft.Json;

using TorrentCat.Models;

namespace TorrentCat
{
    public partial class MainForm : Form
    {
        #region HIWORD & LOWORD

        public static int HIWORD(int n)
        {
            return (n >> 16) & 0xffff;
        }

        public static int HIWORD(IntPtr n)
        {
            return HIWORD(unchecked((int)(long)n));
        }

        public static int LOWORD(int n)
        {
            return n & 0xffff;
        }

        public static int LOWORD(IntPtr n)
        {
            return LOWORD(unchecked((int)(long)n));
        }

        #endregion

        #region WndProc Commands

        const int WM_ACTIVATEAPP = 0x001C;
        const int WM_ACTIVATE = 0x0006;
        const int WM_NCACTIVATE = 0x0086;
        const int WM_SYSCOMMAND = 0x0112;
        const int SC_MINIMIZE = 0xF020;

        #endregion

        #region Symbols for buttons

        public string PLAY_SYMBOL = "\uE768";
        public string PAUSE_SYMBOL = "\uE769";
        public string VOLUME_SYMBOL = "\uE767";
        public string MUTE_SYMBOL = "\uE74F";
        public string FULL_SCREEN_SYMBOL = "\uE740";
        public string OPEN_NEW_WINDOW_SYMBOL = "\uE8A7";
        public string BACK_TO_WINDOW_SYMBOL = "\uE73F";
        public string RETURN_TO_WINDOW_SYMBOL = "\uE944";
        public string CLOSE_SYMBOL = "\uE711";
        public string RESTORE_SYMBOL = "\uE923";
        public string MAXIMIZE_SYMBOL = "\uE922";


        #endregion

        private static string _baseURL { get; } = "http://localhost:8181";
        private string _requestURL = $"{_baseURL}/api/downloads";
        private readonly ChromiumWebBrowser Browser;
        private static Form splashScreen;

        private CancellationTokenSource _cancellationTokenSource;
        private static ClientEngine _engine = null;
        private static StreamProvider _provider = null;
        private FormWindowState _lastState;
        private System.Timers.Timer _moveTimer = new System.Timers.Timer(5000);
        private System.Timers.Timer _infoChecker = new System.Timers.Timer(10000);
        private bool _closed = false;
        private bool _playerMaximized = false;

        private enum FrameSize { Minimized, Maximized };
        private FrameSize VideoContainerState = FrameSize.Maximized;

        private List<TorrentItem> Downloads = new List<TorrentItem>();

        #region Properties

        private LibVLC _libVLC = null;
        public LibVLC LibVLC
        {
            get
            {
                if (_libVLC == null)
                {
                    Core.Initialize();
                    _libVLC = new LibVLC();
                }
                return _libVLC;
            }
            set { _libVLC = value; }
        }

        private MediaPlayer _mediaPlayer = null;
        public MediaPlayer MediaPlayer
        {
            get
            {
                if (_mediaPlayer == null)
                    _mediaPlayer = new MediaPlayer(LibVLC);
                return _mediaPlayer;
            }
            set
            {
                _mediaPlayer = value;
            }
        }

        private int CurrentVideoPosition { get; set; } = 0;

        public string MovieDurationString
        {
            get
            {
                var length = MediaPlayer.Length;
                var time = MediaPlayer.Time;
                var videoTime = "00:00 / 00:00";

                if (length >= 3600000)
                    videoTime = $"{TimeSpan.FromMilliseconds(time):hh\\:mm\\:ss} / {TimeSpan.FromMilliseconds(length):hh\\:mm\\:ss}";
                else if (length == -1)
                    videoTime = TimeSpan.FromMilliseconds(0).ToString(@"mm\:ss");
                else
                    videoTime = $"{TimeSpan.FromMilliseconds(time):mm\\:ss} / {TimeSpan.FromMilliseconds(length):mm\\:ss}";

                return videoTime;
            }
        }

        #endregion


        public MainForm()
        {
            // Check for single instanse of program
            var thisprocessname = Process.GetCurrentProcess().ProcessName;

            if (Process.GetProcesses().Count(p => p.ProcessName == thisprocessname) > 4)
            {
                Console.WriteLine("TorrentCat already running.");
                Program.AlreadyRunning = true;
                return;
            }

            //Create and show splash screen
            splashScreen = new SplashScreen();
            splashScreen.ShowInTaskbar = false;
            var splashThread = new Thread(new ThreadStart(() => Application.Run(splashScreen)));
            splashThread.SetApartmentState(ApartmentState.STA);
            splashThread.Start();

            #region Timers

            // Create information timer (check active downloads count)
            _infoChecker.AutoReset = true;
            _infoChecker.Interval = 10000;
            _infoChecker.Elapsed += OnCheckDownloadsTimerAsync;
            _infoChecker.Start();

            #endregion

            // Initialize form
            InitializeComponent();
            DoubleBuffered = true;

            // Load Segoe MDL2 Assets font
            var pfc = new PrivateFontCollection();
            pfc.AddFontFile($"{AppDomain.CurrentDomain.BaseDirectory}segmdl2.ttf");

            Browser = new ChromiumWebBrowser(_baseURL);
            MainContainer.Controls.Add(Browser);

            #region Set Events Handlers

            // Form
            this.FormClosing += OnFormClosing;
            this.Layout += OnLayoutChanged;

            // Browser
            Browser.AddressChanged += OnBrowserAddressChanged;
            Browser.FrameLoadEnd += OnBrowserFrameLoadEnd;
            Browser.IsBrowserInitializedChanged += (s, e) =>
            {
                if (Browser.IsBrowserInitialized)
                {}
            };
            Browser.LoadError += (sender, args) =>
            {
                Debug.WriteLine($"Browser error: {args.ErrorCode} {args.ErrorText} on: {args.FailedUrl}");
                Browser.Load($"file://{AppDomain.CurrentDomain.BaseDirectory.Replace(@"\", "/")}404.html");
            };

            //Click
            TitleBarButtonExit.Click += TitleBarButton_MouseClick;
            TitleBarButtonMaximizeRestore.Click += TitleBarButton_MouseClick;
            TitleBarButtonMinimize.Click += TitleBarButton_MouseClick;
            //MouseEnter
            TitleBarButtonExit.MouseEnter += TitleBarButton_MouseEnter;
            TitleBarButtonMaximizeRestore.MouseEnter += TitleBarButton_MouseEnter;
            TitleBarButtonMinimize.MouseEnter += TitleBarButton_MouseEnter;
            //MouseDown
            MainContainer.MouseDown += MoveFormMouseDown;
            TitleBarContainer.MouseDown += MoveFormMouseDown;
            TitleBarButtonExit.MouseDown += TitleBarButton_MouseDown;
            TitleBarButtonMaximizeRestore.MouseDown += TitleBarButton_MouseDown;
            TitleBarButtonMinimize.MouseDown += TitleBarButton_MouseDown;
            //MouseLeave
            TitleBarButtonExit.MouseLeave += TitleBarButton_MouseLeave;
            TitleBarButtonMaximizeRestore.MouseLeave += TitleBarButton_MouseLeave;
            TitleBarButtonMinimize.MouseLeave += TitleBarButton_MouseLeave;
            //MouseUp
            TitleBarButtonExit.MouseUp += TitleBarButton_MouseUp;
            TitleBarButtonMaximizeRestore.MouseUp += TitleBarButton_MouseUp;
            TitleBarButtonMinimize.MouseUp += TitleBarButton_MouseUp;


            #endregion
        }

        #region WndProc and movement

        [System.Security.Permissions.PermissionSet(System.Security.Permissions.SecurityAction.Demand, Name = "FullTrust")]
        protected override void WndProc(ref Message m)
        {
            const uint WM_NCHITTEST = 0x0084;
            const uint WM_MOUSEMOVE = 0x0200;

            const uint HTLEFT = 10;
            const uint HTRIGHT = 11;
            const uint HTBOTTOMRIGHT = 17;
            const uint HTBOTTOM = 15;
            const uint HTBOTTOMLEFT = 16;
            const uint HTTOP = 12;
            const uint HTTOPLEFT = 13;
            const uint HTTOPRIGHT = 14;

            const int RESIZE_HANDLE_SIZE = 10;

            bool handled = false;
            if (m.Msg == WM_NCHITTEST || m.Msg == WM_MOUSEMOVE)
            {
                Size formSize = this.Size;
                Point screenPoint = new Point(m.LParam.ToInt32());
                Point clientPoint = this.PointToClient(screenPoint);

                Dictionary<UInt32, Rectangle> boxes = new Dictionary<UInt32, Rectangle>() {
                    {HTBOTTOMLEFT, new Rectangle(0, formSize.Height - RESIZE_HANDLE_SIZE, RESIZE_HANDLE_SIZE, RESIZE_HANDLE_SIZE)},
                    {HTBOTTOM, new Rectangle(RESIZE_HANDLE_SIZE, formSize.Height - RESIZE_HANDLE_SIZE, formSize.Width - 2*RESIZE_HANDLE_SIZE, RESIZE_HANDLE_SIZE)},
                    {HTBOTTOMRIGHT, new Rectangle(formSize.Width - RESIZE_HANDLE_SIZE, formSize.Height - RESIZE_HANDLE_SIZE, RESIZE_HANDLE_SIZE, RESIZE_HANDLE_SIZE)},
                    {HTRIGHT, new Rectangle(formSize.Width - RESIZE_HANDLE_SIZE, RESIZE_HANDLE_SIZE, RESIZE_HANDLE_SIZE, formSize.Height - 2*RESIZE_HANDLE_SIZE)},
                    {HTTOPRIGHT, new Rectangle(formSize.Width - RESIZE_HANDLE_SIZE, 0, RESIZE_HANDLE_SIZE, RESIZE_HANDLE_SIZE) },
                    {HTTOP, new Rectangle(RESIZE_HANDLE_SIZE, 0, formSize.Width - 2*RESIZE_HANDLE_SIZE, RESIZE_HANDLE_SIZE) },
                    {HTTOPLEFT, new Rectangle(0, 0, RESIZE_HANDLE_SIZE, RESIZE_HANDLE_SIZE) },
                    {HTLEFT, new Rectangle(0, RESIZE_HANDLE_SIZE, RESIZE_HANDLE_SIZE, formSize.Height - 2*RESIZE_HANDLE_SIZE) }
                 };

                foreach (KeyValuePair<UInt32, Rectangle> hitBox in boxes)
                {
                    if (hitBox.Value.Contains(clientPoint))
                    {
                        m.Result = (IntPtr)hitBox.Key;
                        handled = true;
                        break;
                    }
                }
            }

            if (!handled)
                base.WndProc(ref m);
        }

        private void MoveFormMouseDown(object sender, MouseEventArgs e)
        {
            const int WM_NCLBUTTONDOWN = 0xA1;
            const int HT_CAPTION = 0x2;

            (sender as Control).Capture = false;
            Message msg = Message.Create(Handle, WM_NCLBUTTONDOWN, (IntPtr)HT_CAPTION, IntPtr.Zero);
            WndProc(ref msg);
        }


        #endregion

        #region Command buttons Handlers

        readonly Color CloseButtonHovered = Color.FromArgb(232, 17, 35),
            CloseButtonPressed = Color.FromArgb(241, 112, 122),
            MaximizeRestoreMinimizeButtonHover = Color.FromArgb(64, 64, 64),
            MaximizeRestoreMinimizeButtonPressed = Color.FromArgb(202, 202, 203);

        private void TitleBarButton_MouseEnter(object sender, EventArgs e)
        {
            if (sender as Control == TitleBarButtonExit)
                (sender as Control).BackColor = CloseButtonHovered;
            else
                (sender as Control).BackColor = MaximizeRestoreMinimizeButtonHover;
        }

        private void TitleBarButton_MouseDown(object sender, EventArgs e)
        {
            if (sender as Control == TitleBarButtonExit)
                (sender as Control).BackColor = CloseButtonPressed;
            else
                (sender as Control).BackColor = MaximizeRestoreMinimizeButtonPressed;
        }

        private void TitleBarButton_MouseClick(object sender, EventArgs e)
        {
            if (sender as Control == TitleBarButtonExit)
            {
                if (MediaPlayer.IsPlaying)
                {
                    StopPlayer();
                    VideoContainer.Visible = false;
                }
                Visible = false;
            }
            else if (sender as Control == TitleBarButtonMaximizeRestore)
            {
                if (WindowState == FormWindowState.Maximized)
                    WindowState = FormWindowState.Normal;
                else
                    WindowState = FormWindowState.Maximized;
            }
            else if (sender as Control == TitleBarButtonMinimize)
                WindowState = FormWindowState.Minimized;
        }

        private void TitleBarButton_MouseLeave(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.Black;
        }

        private void TitleBarButton_MouseUp(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.Black;
        }

        #endregion

        #region Class methods

        internal static Size GetScreenResolution()
        {
            Screen myScreen = Screen.FromPoint(Cursor.Position);
            System.Drawing.Rectangle area = myScreen.WorkingArea;
            return new Size((int)area.Width, (int)area.Height);
        }

        protected override void SetVisibleCore(bool value)
        {
            if (value)
            {
                CloseWindowMenuItem.Enabled = true;
                OpenWindowMenuItem.Enabled = false;
            }
            else
            {
                CloseWindowMenuItem.Enabled = false;
                OpenWindowMenuItem.Enabled = true;
            }
            base.SetVisibleCore(value);
        }

        public async Task PlayVideo(string uri)
        {
            if (MediaPlayer != null && MediaPlayer.IsPlaying)
            {
                _cancellationTokenSource.Cancel();
                _cancellationTokenSource.Dispose();
                MediaPlayer.Stop();
            }
            _cancellationTokenSource = new CancellationTokenSource();

            Program.MainWindow.Invoke((MethodInvoker)async delegate ()
            {
                if (VideoView.MediaPlayer != null && VideoView.MediaPlayer.IsPlaying)
                    VideoView.MediaPlayer.Stop();
                else
                    VideoView.MediaPlayer = MediaPlayer;

                MediaPlayer.VolumeChanged += OnMediaPlayerVolumeChanged;
                MediaPlayer.LengthChanged += OnMediaPlayerLengthChanged;
                MediaPlayer.PositionChanged += OnMediaPlayerPositionChanged;
                MediaPlayer.Playing += OnMediaPlayerPlaying;

                VideoContainer.Visible = true;
                VideoView.MediaPlayer.EnableMouseInput = false;
                VideoView.MediaPlayer.EnableKeyInput = false;
                VolumeTrackBar.Value = 100;

                try
                {
                    var media = uri.Contains(".torrent")
                        ? new Media(LibVLC, new StreamMediaInput(await StartTorrenting(uri, _cancellationTokenSource.Token)))
                        : new Media(LibVLC, new Uri(uri));
                    if (!MediaPlayer.Play(media))
                    {
                        Debug.WriteLine($"Failed to open media: {uri}");
                        StopPlayer();
                        return;
                    }
                }
                catch (Exception ex)
                {
                    Debug.WriteLine($"Failed to open media with error: {ex.Message}");
                    StopPlayer();
                    return;
                }

                _closed = false;
            });

            await Task.Delay(25);
        }

        private static async Task<Stream> StartTorrenting(string uri, CancellationToken ctoken)
        {
            bool verbose = false;
            string downloadPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.UserProfile), "Downloads");

            var parts = WebUtility.UrlDecode(uri).Split('/');
            var tfname = parts.Where(s => s.Contains(".torrent")).ToList()[0];
            var settings = new EngineSettings
            {
                PreferEncryption = true,
                SavePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.UserProfile), "Downloads").Replace(@"\", "/")
            };

            _engine = new ClientEngine(settings);

            Debug.WriteLine($"MonoTorrent -> Loading torrent file {tfname}");
            var torrent = Torrent.Load(new Uri(uri), Path.Combine(downloadPath, tfname));

            Debug.WriteLine("MonoTorrent -> Creating a new StreamProvider...");
            _provider = new StreamProvider(_engine, downloadPath, torrent);

            foreach (var t in _provider.Manager.Torrent.Files)
                t.Priority = Priority.Normal;

            if (verbose)
            {
                _provider.Manager.PeerConnected += (o, e) => Debug.WriteLine($"MonoTorrent -> Connection succeeded: {e.Peer.Uri}");
                _provider.Manager.ConnectionAttemptFailed += (o, e) => Debug.WriteLine($"MonoTorrent -> Connection failed: {e.Peer.ConnectionUri} - {e.Reason} - {e.Peer.AllowedEncryption}");
            }

            // Disable rarest first and randomised picking - only allow priority based picking (i.e. selective downloading)
            PiecePicker picker = new StandardPicker();
            picker = new PriorityPicker(picker);
            await _provider.Manager.ChangePickerAsync(picker);

            Debug.WriteLine("MonoTorrent -> Starting the StreamProvider...");
            await _provider.StartAsync();

            Debug.WriteLine("MonoTorrent -> Creating a stream from the torrent file...");
            var stream = await _provider.CreateStreamAsync(_provider.Manager.Torrent.Files[0], ctoken);

            return stream;
        }


        private void StopPlayer()
        {
            Program.MainWindow.Invoke(new Action(() =>
            {
                if (VideoContainer.Visible)
                    VideoContainer.Visible = false;
            }));

            _cancellationTokenSource?.Cancel();

            MediaPlayer.VolumeChanged -= OnMediaPlayerVolumeChanged;
            MediaPlayer.LengthChanged -= OnMediaPlayerLengthChanged;
            MediaPlayer.PositionChanged -= OnMediaPlayerPositionChanged;
            MediaPlayer.Playing -= OnMediaPlayerPlaying;

            _ = Task.Run(async () =>
            {
                MediaPlayer.Stop();
                if (_provider != null && _provider.Active)
                {
                    await _provider.StopAsync();
                    _provider.Manager.Dispose();
                    _engine.Dispose();
                    _provider = null;
                }
                MediaPlayer.Media?.Dispose();
            }).ConfigureAwait(false);
        }

        #endregion

        #region Events Handlers

        protected override void OnLoad(EventArgs args)
        {
            MinimumSize = new Size(1037, 646);
            // Set size and position
            var initialSize = GetScreenResolution();
            initialSize.Width = (int)(initialSize.Width * 0.8);
            initialSize.Height = (int)(initialSize.Height * 0.8);
            ClientSize = initialSize;
            SetDesktopLocation((GetScreenResolution().Width - initialSize.Width) / 2,
                (GetScreenResolution().Height - initialSize.Height) / 2);

            VideoContainer.Visible = false;
            _closed = false;
            _lastState = WindowState;

            base.OnLoad(args);
        }

        private void OnFormClosing(object sender, CancelEventArgs e)
        {
            // Stop splash screen
            if (splashScreen != null && !splashScreen.Disposing && !splashScreen.IsDisposed)
                splashScreen.Invoke(new Action(() => splashScreen.Close()));


            Program.ApiProcess.Stop();
            _infoChecker.Stop();
            _infoChecker.Dispose();

            Hide();
            ShowInTaskbar = false;

            if (MediaPlayer.IsPlaying)
            {
                StopPlayer();
            }
            
            Cef.Shutdown();

            e.Cancel = false;
        }

        private void OnBrowserAddressChanged(object sender, AddressChangedEventArgs args)
        {
            if (args.Address.Contains("/play?uri="))
            {
                var uri = WebUtility.UrlDecode(args.Address.Substring(args.Address.IndexOf("?uri=") + 5));
                Debug.WriteLine($"Start playing: {uri}");
                _ = Task.Run(() => PlayVideo(uri)).ConfigureAwait(false);
            }
            else if (MediaPlayer.IsPlaying)
            {
                Debug.WriteLine($"Stop playing");
                if (VideoContainerState == FrameSize.Maximized)
                    Program.MainWindow.Invoke(new Action(() => OnFullWindowButtonClick(this, new EventArgs())));
            }
        }

        private void OnBrowserFrameLoadEnd(object sender, FrameLoadEndEventArgs e)
        {
            // Stop splash screen
            if (splashScreen != null && !splashScreen.Disposing && !splashScreen.IsDisposed)
                splashScreen.Invoke(new Action(() => splashScreen.Close()));

            Program.MainWindow.Invoke((MethodInvoker)delegate ()
            {
                ShowInTaskbar = true;
                WindowState = FormWindowState.Normal;
            });

            Browser.FrameLoadEnd -= OnBrowserFrameLoadEnd;
        }

        private void OnLayoutChanged(object sender, LayoutEventArgs args)
        {
            if (_lastState != WindowState && args.AffectedControl.Name == "MainForm")
            {
                _lastState = WindowState;

                if (WindowState == FormWindowState.Maximized)
                {
                    TitleBarButtonMaximizeRestore.Text = RESTORE_SYMBOL;
                    Padding = new Padding(0, 0, 0, 0);
                }
                else if (WindowState == FormWindowState.Normal)
                {
                    TitleBarButtonMaximizeRestore.Text = MAXIMIZE_SYMBOL;
                    Padding = new Padding(5, 5, 5, 5);
                }
            }
        }

        private async void OnCheckDownloadsTimerAsync(object s, EventArgs e)
        {
            var downloadItems = new List<TorrentItem>();
            string responseBody;

            using (var client = new HttpClient())
            {
                try
                {
                    client.Timeout = TimeSpan.FromSeconds(12);
                    var response = await client.GetAsync(_requestURL);
                    response.EnsureSuccessStatusCode();
                    responseBody = await response.Content.ReadAsStringAsync();
                }
                catch (Exception ex)
                {
                    Debug.WriteLine($"Failed to get active downloads from API with error: {ex.Message}");
                    return;
                }
            }

            try
            {
                downloadItems = JsonConvert.DeserializeObject<List<TorrentItem>>(responseBody);
            }
            catch (JsonSerializationException ex)
            {
                Debug.WriteLine(ex.Message);
                return;
            }

            if (downloadItems == null) return;
            var downloading = downloadItems.FindAll(k => k.Status.ToLower() == "downloading");

            var loaded = Downloads.Except(downloading).ToList();
            Downloads = downloadItems;

            try
            {
                if (Program.MainWindow.IsHandleCreated)
                {
                    Program.MainWindow.Invoke((MethodInvoker)delegate ()
                    {
                        ActiveDownloadsMenuItem.Text = $"Активные загрузки - {downloading.Count}";
                    });

                    if (loaded.Count == 0) return;

                    foreach (var di in loaded)
                    {
                        //Program.MainWindow.Invoke((MethodInvoker)delegate ()
                        //{
                        //    NotifyIcon.ShowBalloonTip(3000,
                        //    "TorrentCat",
                        //    $"Загружен {di.Name}",
                        //    NotifyIcon.BalloonTipIcon
                        //    );
                        //});

                    }
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine($"Controls disposed or not initialized: {ex.Message}");
            }
        }

        private void OnPlayButtonClick(object sender, EventArgs e)
        {
            _ = Task.Run(() =>
            {
                if (MediaPlayer.IsPlaying)
                {
                    MediaPlayer.Pause();
                    Program.MainWindow.Invoke(new Action(() => PlayButton.Text = PLAY_SYMBOL));
                }
                else
                {
                    MediaPlayer.Play();
                    Program.MainWindow.Invoke(new Action(() => PlayButton.Text = PAUSE_SYMBOL));
                }
            }).ConfigureAwait(false);
        }

        private void OnVolumeButtonClick(object sender, EventArgs e)
        {
            _ = Task.Run(() =>
            {
                if (!MediaPlayer.Mute)
                {
                    MediaPlayer.Mute = true;
                    Program.MainWindow.Invoke(new Action(() => VolumeButton.Text = MUTE_SYMBOL));
                }
                else
                {
                    MediaPlayer.Mute = false;
                    Program.MainWindow.Invoke(new Action(() => VolumeButton.Text = VOLUME_SYMBOL));
                }
            }).ConfigureAwait(false);
        }


        private Size _playerSize;
        private Point _playerLocation;
        private void OnFullScreenButtonClick(object sender, EventArgs e)
        {
            if (!MediaPlayer.Fullscreen)
            {
                _playerSize = VideoContainer.Size;
                _playerLocation = VideoContainer.Location;
                MediaPlayer.Fullscreen = true;
                FullScreenButton.Text = BACK_TO_WINDOW_SYMBOL;
                WindowState = FormWindowState.Maximized;
                VideoContainer.Location = new Point(0, 0);
                VideoContainer.Height = Height;
                VideoContainer.Width = Width;
                TitleBarContainer.Hide();
                _playerMaximized = true;
                FullWindowButton.Enabled = false;
            }
            else
            {
                MediaPlayer.Fullscreen = false;
                FullScreenButton.Text = FULL_SCREEN_SYMBOL;
                WindowState = FormWindowState.Normal;
                VideoContainer.Size = _playerSize;
                VideoContainer.Location = _playerLocation;
                TitleBarContainer.Show();
                _playerMaximized = false;
                FullWindowButton.Enabled = true;
            }
        }

        private void OnCloseButtonClick(object sender, EventArgs e)
        {
            _closed = true;
            _moveTimer.Stop();
            StopPlayer();
            VideoContainer.Visible = false;
            _cancellationTokenSource?.Dispose();
            if (_playerMaximized)
            {
                TitleBarContainer.Show();
                TitleBarButtonExit.Show();
                TitleBarButtonMaximizeRestore.Show();
                TitleBarButtonMinimize.Show();
                _playerMaximized = false;
            }
        }

        private void OnVideoTrackBarValueChanged(object sender, EventArgs e)
        {
            _ = Task.Run(() =>
            {
                Program.MainWindow.Invoke(new Action(() =>
                {
                    if (CurrentVideoPosition != VideoTrackBar.Value)
                        MediaPlayer.Time = VideoTrackBar.Value;
                }));
            }).ConfigureAwait(false);
        }

        private void OnVolumeTrackBarValueChanged(object sender, EventArgs e)
        {
            _ = Task.Run(() =>
                Program.MainWindow.Invoke(new Action(() => MediaPlayer.Volume = VolumeTrackBar.Value))
                ).ConfigureAwait(false);
        }

        private void OnPlayerMouseEnter(object sender, EventArgs e)
        {
            PlayControlPanel.Visible = true;
        }

        private void OnPlayerMouseLeave(object sender, EventArgs e)
        {
            _moveTimer.Stop();
            if (VideoContainer.ClientRectangle.Contains(VideoContainer.PointToClient(Control.MousePosition)))
            {
                // the mouse is inside the control bounds
            }
            else
            {
                // the mouse is outside the control bounds
                PlayControlPanel.Visible = false;
            }
        }

        private void OnPlayerMouseMove(object sender, MouseEventArgs e)
        {
            if (_closed) return;

            if (!_moveTimer.Enabled)
            {
                _moveTimer.AutoReset = false;
                _moveTimer.Elapsed += (s, a) => Program.MainWindow.Invoke(new Action (() => PlayControlPanel.Visible = false));
                _moveTimer.Start();
            }
            else
            {
                _moveTimer.Stop();
                _moveTimer.Enabled = true;
                _moveTimer.Start();
                PlayControlPanel.Visible = true;
            }
        }

        private void OnMediaPlayerPlaying(object sender, EventArgs e)
        {
            Program.MainWindow.Invoke(new Action (() => PlayButton.Text = PAUSE_SYMBOL));
        }

        private void OnMediaPlayerVolumeChanged(object sender, EventArgs e)
        {
            Program.MainWindow.Invoke(new Action(() => VolumeTrackBar.Value = (int)MediaPlayer?.Volume));
        }

        private void OnMediaPlayerLengthChanged(object sender, EventArgs e)
        {
            Program.MainWindow.Invoke(new Action(() =>
            {
                CurrentVideoPosition = (int)MediaPlayer?.Time;
                VideoTrackBar.MaxValue = (int)MediaPlayer?.Length;
                VideoTrackBar.Value = (int)MediaPlayer?.Time;
                VideoTimeLabel.Text = MovieDurationString;
            }));
        }

        private void OnMediaPlayerPositionChanged(object sender, EventArgs e)
        {
            try
            {
                Program.MainWindow.Invoke(new Action(() =>
                {
                    CurrentVideoPosition = (int)MediaPlayer?.Time;
                    VideoTrackBar.Value = (int)MediaPlayer?.Time;
                    VideoTimeLabel.Text = MovieDurationString;
                }));
            }
            catch (InvalidAsynchronousStateException)
            {

            }
        }


        private void OnNotifyIconCloseWindow(object sender, EventArgs e)
        {
            if (MediaPlayer.IsPlaying)
            {
                StopPlayer();
                VideoContainer.Visible = false;
            }
            Hide();
            ShowInTaskbar = false;
        }

        private void OnNotifyIconOpenWindow(object sender, EventArgs e)
        {
            ShowInTaskbar = true;
            Show();
        }

        private void OnTitleBarDoubleClick(object sender, EventArgs e)
        {
            if (WindowState == FormWindowState.Normal)
                WindowState = FormWindowState.Maximized;
            else
                WindowState = FormWindowState.Normal;
        }

        private void OnFullWindowButtonClick(object sender, EventArgs e)
        {
            FullWindowButton.Text = FullWindowButton.Text == OPEN_NEW_WINDOW_SYMBOL
                ? RETURN_TO_WINDOW_SYMBOL
                : OPEN_NEW_WINDOW_SYMBOL;

            if (VideoContainerState == FrameSize.Maximized)
            {
                uint mWidth = 0, mHeight = 0;
                VideoContainer.Anchor = AnchorStyles.Left | AnchorStyles.Top;


                MediaPlayer.Size(0, ref mWidth, ref mHeight);
                var aspect = (double)mWidth / (double)(mHeight);
                var newWidth = (int)((double)VideoContainer.Width / 2.2361);
                var newHieght = (int)((double)newWidth / aspect);
                VideoContainer.Location = new Point(
                    VideoContainer.Location.X + (VideoContainer.Width - newWidth),
                    VideoContainer.Location.Y + (VideoContainer.Height - newHieght)
                    );
                VideoContainer.Size = new Size(newWidth, newHieght);

                VideoContainer.Anchor = AnchorStyles.Right | AnchorStyles.Bottom;
                VideoContainer.AutoSize = false;
            }
            else
            {
                VideoContainer.Size = new Size(Width - 60, Height - 105);
                VideoContainer.Location = new Point(55, 100);
                VideoContainer.Anchor = AnchorStyles.Left | AnchorStyles.Top | AnchorStyles.Right | AnchorStyles.Bottom;
                VideoContainer.AutoSize = true;
            }

            VideoContainerState = VideoContainerState == FrameSize.Maximized ? FrameSize.Minimized : FrameSize.Maximized;
        }

        private void OnNotifyIconDoubleClick(object sender, MouseEventArgs e)
        {
            if (!Visible)
            {
                ShowInTaskbar = true;
                Show();
            }
        }

        private void OnNotifyIconDownloads(object sender, EventArgs e)
        {
            if (!Visible)
            {
                ShowInTaskbar = true;
                Show();
            }
            Browser.Load($"{_baseURL}/downloads");
        }

        private void OnNotifyIconAuthorize(object sender, EventArgs e)
        {
            if (!Visible)
            {
                ShowInTaskbar = true;
                Show();
            }
            Browser.Load($"{_baseURL}/profile/login");
        }

        private void OnNotifyIconExit(object sender, EventArgs e)
        {
            Program.ApiProcess.Stop();

            if (MediaPlayer.IsPlaying)
            {
                StopPlayer();
            }

            Thread.Sleep(2000);
            Close();
        }

        #endregion

    }
}
