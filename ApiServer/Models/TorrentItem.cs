﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApiServer.Models
{
    public class TorrentItem
	{
		public int ID { get; set; }
		public string Status { get; set; }
		public long Length { get; set; }
		public string Name { get; set; }
		public string Quality { get; set; }
		public long BytesCompleted { get; set; }
		public string File { get; set; }
		public int Seeds { get; set; }
		public List<FileInfo> Files { get; } = new List<FileInfo>();
		public MovieItem Movie { get; set; } = new MovieItem();
    }

	public class MovieItem
	{
		public int ID { get; set; }
		public int OrigID { get; set; }
		public string ImgV { get; set; }
		public string ImgH { get; set; }
		public string Title { get; set; }
		public string TitleOrig { get; set; }
		public string Description { get; set; }
		public double ContentRating { get; set; }
		public double Duration { get; set; }
		public List<string> Genres { get; set; } = new List<string>();
		public List<string> Countries { get; set; } = new List<string>();
		public string Type { get; set; }
		public int Year { get; set; }
		public string Player { get; set; }
		public Dictionary<string, double> Ratings { get; set; } = new Dictionary<string, double>();
	}

	public class FileInfo
	{
		public string Path { get; set; }
		public long BytesCompleted { get; set; }
		public long Length { get; set; }
    }
}
