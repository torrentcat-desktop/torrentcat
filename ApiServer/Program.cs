using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.DependencyInjection;
using ApiServer.Services;

namespace ApiServer
{
    public static class Program
    {
        public static IHost host;

        public static void Main(string[] args)
        {
            host = CreateHostBuilder(args).Build();
            _ = Task.Run(() => host.Services.GetRequiredService<Downloader>().CheckTorrents()).ConfigureAwait(false);
            host.Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder
                        .UseContentRoot(AppContext.BaseDirectory)
                        .UseKestrel()
                        .UseUrls("http://localhost:8181")
                        .UseShutdownTimeout(TimeSpan.FromSeconds(3))
                        .UseStartup<Startup>();
                });
    }
}
