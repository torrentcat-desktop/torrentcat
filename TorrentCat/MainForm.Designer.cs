﻿namespace TorrentCat
{
    partial class MainForm
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            gTrackBar.ColorPack colorPack1 = new gTrackBar.ColorPack();
            gTrackBar.ColorPack colorPack2 = new gTrackBar.ColorPack();
            gTrackBar.ColorPack colorPack3 = new gTrackBar.ColorPack();
            gTrackBar.ColorPack colorPack4 = new gTrackBar.ColorPack();
            gTrackBar.ColorPack colorPack5 = new gTrackBar.ColorPack();
            gTrackBar.ColorPack colorPack6 = new gTrackBar.ColorPack();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.MainContainer = new System.Windows.Forms.Panel();
            this.TitleBarContainer = new TorrentCat.TransparentPanel();
            this.CommandContainer = new System.Windows.Forms.Panel();
            this.TitleBarButtonMinimize = new System.Windows.Forms.Label();
            this.TitleBarButtonMaximizeRestore = new System.Windows.Forms.Label();
            this.TitleBarButtonExit = new System.Windows.Forms.Label();
            this.VideoContainer = new System.Windows.Forms.Panel();
            this.PlayControlPanel = new System.Windows.Forms.Panel();
            this.UpPanel = new System.Windows.Forms.Panel();
            this.VideoTrackBar = new gTrackBar.gTrackBar();
            this.DownPanel = new System.Windows.Forms.Panel();
            this.FullWindowButton = new System.Windows.Forms.Label();
            this.FullScreenButton = new System.Windows.Forms.Label();
            this.CloseButton = new System.Windows.Forms.Label();
            this.VideoTimeLabel = new System.Windows.Forms.Label();
            this.VolumeTrackBar = new gTrackBar.gTrackBar();
            this.VolumeButton = new System.Windows.Forms.Label();
            this.PlayButton = new System.Windows.Forms.Label();
            this.OverlayControl = new TorrentCat.TransparentControl();
            this.VideoView = new LibVLCSharp.WinForms.VideoView();
            this.NotifyIcon = new System.Windows.Forms.NotifyIcon(this.components);
            this.SysTrayMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.OpenWindowMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.CloseWindowMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.ActiveDownloadsMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.AuthorizationMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.ExitMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.CommandContainer.SuspendLayout();
            this.VideoContainer.SuspendLayout();
            this.PlayControlPanel.SuspendLayout();
            this.UpPanel.SuspendLayout();
            this.DownPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.VideoView)).BeginInit();
            this.SysTrayMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // MainContainer
            // 
            this.MainContainer.BackColor = System.Drawing.Color.Black;
            this.MainContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MainContainer.Location = new System.Drawing.Point(5, 5);
            this.MainContainer.Margin = new System.Windows.Forms.Padding(0);
            this.MainContainer.Name = "MainContainer";
            this.MainContainer.Size = new System.Drawing.Size(790, 440);
            this.MainContainer.TabIndex = 0;
            // 
            // TitleBarContainer
            // 
            this.TitleBarContainer.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TitleBarContainer.BackColor = System.Drawing.Color.Transparent;
            this.TitleBarContainer.Location = new System.Drawing.Point(55, 5);
            this.TitleBarContainer.Margin = new System.Windows.Forms.Padding(0);
            this.TitleBarContainer.Name = "TitleBarContainer";
            this.TitleBarContainer.Opacity = 100;
            this.TitleBarContainer.Size = new System.Drawing.Size(605, 30);
            this.TitleBarContainer.TabIndex = 0;
            this.TitleBarContainer.DoubleClick += new System.EventHandler(this.OnTitleBarDoubleClick);
            // 
            // CommandContainer
            // 
            this.CommandContainer.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.CommandContainer.BackColor = System.Drawing.Color.Black;
            this.CommandContainer.Controls.Add(this.TitleBarButtonMinimize);
            this.CommandContainer.Controls.Add(this.TitleBarButtonMaximizeRestore);
            this.CommandContainer.Controls.Add(this.TitleBarButtonExit);
            this.CommandContainer.Location = new System.Drawing.Point(660, 5);
            this.CommandContainer.Margin = new System.Windows.Forms.Padding(0);
            this.CommandContainer.Name = "CommandContainer";
            this.CommandContainer.Size = new System.Drawing.Size(135, 30);
            this.CommandContainer.TabIndex = 0;
            // 
            // TitleBarButtonMinimize
            // 
            this.TitleBarButtonMinimize.BackColor = System.Drawing.Color.Transparent;
            this.TitleBarButtonMinimize.Dock = System.Windows.Forms.DockStyle.Right;
            this.TitleBarButtonMinimize.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.TitleBarButtonMinimize.Font = new System.Drawing.Font("Segoe MDL2 Assets", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.TitleBarButtonMinimize.ForeColor = System.Drawing.Color.Silver;
            this.TitleBarButtonMinimize.Location = new System.Drawing.Point(0, 0);
            this.TitleBarButtonMinimize.Margin = new System.Windows.Forms.Padding(0);
            this.TitleBarButtonMinimize.Name = "TitleBarButtonMinimize";
            this.TitleBarButtonMinimize.Size = new System.Drawing.Size(45, 30);
            this.TitleBarButtonMinimize.TabIndex = 2;
            this.TitleBarButtonMinimize.Text = "";
            this.TitleBarButtonMinimize.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // TitleBarButtonMaximizeRestore
            // 
            this.TitleBarButtonMaximizeRestore.BackColor = System.Drawing.Color.Transparent;
            this.TitleBarButtonMaximizeRestore.Dock = System.Windows.Forms.DockStyle.Right;
            this.TitleBarButtonMaximizeRestore.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.TitleBarButtonMaximizeRestore.Font = new System.Drawing.Font("Segoe MDL2 Assets", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.TitleBarButtonMaximizeRestore.ForeColor = System.Drawing.Color.Silver;
            this.TitleBarButtonMaximizeRestore.Location = new System.Drawing.Point(45, 0);
            this.TitleBarButtonMaximizeRestore.Margin = new System.Windows.Forms.Padding(0);
            this.TitleBarButtonMaximizeRestore.Name = "TitleBarButtonMaximizeRestore";
            this.TitleBarButtonMaximizeRestore.Size = new System.Drawing.Size(45, 30);
            this.TitleBarButtonMaximizeRestore.TabIndex = 1;
            this.TitleBarButtonMaximizeRestore.Text = "";
            this.TitleBarButtonMaximizeRestore.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // TitleBarButtonExit
            // 
            this.TitleBarButtonExit.BackColor = System.Drawing.Color.Transparent;
            this.TitleBarButtonExit.Dock = System.Windows.Forms.DockStyle.Right;
            this.TitleBarButtonExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.TitleBarButtonExit.Font = new System.Drawing.Font("Segoe MDL2 Assets", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.TitleBarButtonExit.ForeColor = System.Drawing.Color.Silver;
            this.TitleBarButtonExit.Location = new System.Drawing.Point(90, 0);
            this.TitleBarButtonExit.Margin = new System.Windows.Forms.Padding(0);
            this.TitleBarButtonExit.Name = "TitleBarButtonExit";
            this.TitleBarButtonExit.Size = new System.Drawing.Size(45, 30);
            this.TitleBarButtonExit.TabIndex = 0;
            this.TitleBarButtonExit.Text = "";
            this.TitleBarButtonExit.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // VideoContainer
            // 
            this.VideoContainer.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.VideoContainer.AutoSize = true;
            this.VideoContainer.BackColor = System.Drawing.Color.Black;
            this.VideoContainer.Controls.Add(this.PlayControlPanel);
            this.VideoContainer.Controls.Add(this.OverlayControl);
            this.VideoContainer.Controls.Add(this.VideoView);
            this.VideoContainer.Location = new System.Drawing.Point(55, 100);
            this.VideoContainer.Margin = new System.Windows.Forms.Padding(0);
            this.VideoContainer.Name = "VideoContainer";
            this.VideoContainer.Size = new System.Drawing.Size(740, 345);
            this.VideoContainer.TabIndex = 0;
            // 
            // PlayControlPanel
            // 
            this.PlayControlPanel.BackColor = System.Drawing.Color.Black;
            this.PlayControlPanel.Controls.Add(this.UpPanel);
            this.PlayControlPanel.Controls.Add(this.DownPanel);
            this.PlayControlPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.PlayControlPanel.Location = new System.Drawing.Point(0, 295);
            this.PlayControlPanel.Margin = new System.Windows.Forms.Padding(0);
            this.PlayControlPanel.Name = "PlayControlPanel";
            this.PlayControlPanel.Size = new System.Drawing.Size(740, 50);
            this.PlayControlPanel.TabIndex = 2;
            // 
            // UpPanel
            // 
            this.UpPanel.Controls.Add(this.VideoTrackBar);
            this.UpPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.UpPanel.Location = new System.Drawing.Point(0, 0);
            this.UpPanel.Margin = new System.Windows.Forms.Padding(0);
            this.UpPanel.Name = "UpPanel";
            this.UpPanel.Size = new System.Drawing.Size(740, 15);
            this.UpPanel.TabIndex = 0;
            // 
            // VideoTrackBar
            // 
            this.VideoTrackBar.AutoSize = true;
            this.VideoTrackBar.BackColor = System.Drawing.Color.Transparent;
            colorPack1.Border = System.Drawing.Color.Red;
            colorPack1.Face = System.Drawing.Color.Red;
            colorPack1.Highlight = System.Drawing.Color.Red;
            this.VideoTrackBar.ColorDown = colorPack1;
            colorPack2.Border = System.Drawing.Color.Red;
            colorPack2.Face = System.Drawing.Color.Red;
            colorPack2.Highlight = System.Drawing.Color.Red;
            this.VideoTrackBar.ColorHover = colorPack2;
            colorPack3.Border = System.Drawing.Color.Red;
            colorPack3.Face = System.Drawing.Color.Red;
            colorPack3.Highlight = System.Drawing.Color.Red;
            this.VideoTrackBar.ColorUp = colorPack3;
            this.VideoTrackBar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.VideoTrackBar.Dock = System.Windows.Forms.DockStyle.Fill;
            this.VideoTrackBar.FloatValueFontColor = System.Drawing.Color.Silver;
            this.VideoTrackBar.JumpToMouse = true;
            this.VideoTrackBar.Label = null;
            this.VideoTrackBar.Location = new System.Drawing.Point(0, 0);
            this.VideoTrackBar.Margin = new System.Windows.Forms.Padding(0);
            this.VideoTrackBar.MaxValue = 100;
            this.VideoTrackBar.Name = "VideoTrackBar";
            this.VideoTrackBar.ShowFocus = false;
            this.VideoTrackBar.Size = new System.Drawing.Size(740, 15);
            this.VideoTrackBar.SliderSize = new System.Drawing.Size(10, 10);
            this.VideoTrackBar.SliderWidthHigh = 3F;
            this.VideoTrackBar.SliderWidthLow = 3F;
            this.VideoTrackBar.TabIndex = 0;
            this.VideoTrackBar.TabStop = false;
            this.VideoTrackBar.TickThickness = 1F;
            this.VideoTrackBar.UpDownShow = false;
            this.VideoTrackBar.Value = 0;
            this.VideoTrackBar.ValueAdjusted = 0F;
            this.VideoTrackBar.ValueDivisor = gTrackBar.gTrackBar.eValueDivisor.e1;
            this.VideoTrackBar.ValueStrFormat = null;
            this.VideoTrackBar.ValueChanged += new gTrackBar.gTrackBar.ValueChangedEventHandler(this.OnVideoTrackBarValueChanged);
            // 
            // DownPanel
            // 
            this.DownPanel.Controls.Add(this.FullWindowButton);
            this.DownPanel.Controls.Add(this.FullScreenButton);
            this.DownPanel.Controls.Add(this.CloseButton);
            this.DownPanel.Controls.Add(this.VideoTimeLabel);
            this.DownPanel.Controls.Add(this.VolumeTrackBar);
            this.DownPanel.Controls.Add(this.VolumeButton);
            this.DownPanel.Controls.Add(this.PlayButton);
            this.DownPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.DownPanel.Location = new System.Drawing.Point(0, 15);
            this.DownPanel.Margin = new System.Windows.Forms.Padding(0);
            this.DownPanel.Name = "DownPanel";
            this.DownPanel.Size = new System.Drawing.Size(740, 35);
            this.DownPanel.TabIndex = 1;
            // 
            // FullWindowButton
            // 
            this.FullWindowButton.BackColor = System.Drawing.Color.Transparent;
            this.FullWindowButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.FullWindowButton.Dock = System.Windows.Forms.DockStyle.Right;
            this.FullWindowButton.Font = new System.Drawing.Font("Segoe MDL2 Assets", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.FullWindowButton.ForeColor = System.Drawing.Color.Silver;
            this.FullWindowButton.Location = new System.Drawing.Point(617, 0);
            this.FullWindowButton.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.FullWindowButton.Name = "FullWindowButton";
            this.FullWindowButton.Size = new System.Drawing.Size(41, 35);
            this.FullWindowButton.TabIndex = 2;
            this.FullWindowButton.Text = "";
            this.FullWindowButton.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.FullWindowButton.Click += new System.EventHandler(this.OnFullWindowButtonClick);
            // 
            // FullScreenButton
            // 
            this.FullScreenButton.BackColor = System.Drawing.Color.Transparent;
            this.FullScreenButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.FullScreenButton.Dock = System.Windows.Forms.DockStyle.Right;
            this.FullScreenButton.Font = new System.Drawing.Font("Segoe MDL2 Assets", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.FullScreenButton.ForeColor = System.Drawing.Color.Silver;
            this.FullScreenButton.Location = new System.Drawing.Point(658, 0);
            this.FullScreenButton.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.FullScreenButton.Name = "FullScreenButton";
            this.FullScreenButton.Size = new System.Drawing.Size(41, 35);
            this.FullScreenButton.TabIndex = 2;
            this.FullScreenButton.Text = "";
            this.FullScreenButton.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.FullScreenButton.Click += new System.EventHandler(this.OnFullScreenButtonClick);
            // 
            // CloseButton
            // 
            this.CloseButton.BackColor = System.Drawing.Color.Transparent;
            this.CloseButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.CloseButton.Dock = System.Windows.Forms.DockStyle.Right;
            this.CloseButton.Font = new System.Drawing.Font("Segoe MDL2 Assets", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.CloseButton.ForeColor = System.Drawing.Color.Silver;
            this.CloseButton.Location = new System.Drawing.Point(699, 0);
            this.CloseButton.Margin = new System.Windows.Forms.Padding(0);
            this.CloseButton.Name = "CloseButton";
            this.CloseButton.Size = new System.Drawing.Size(41, 35);
            this.CloseButton.TabIndex = 0;
            this.CloseButton.Text = "";
            this.CloseButton.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.CloseButton.Click += new System.EventHandler(this.OnCloseButtonClick);
            // 
            // VideoTimeLabel
            // 
            this.VideoTimeLabel.BackColor = System.Drawing.Color.Transparent;
            this.VideoTimeLabel.Dock = System.Windows.Forms.DockStyle.Left;
            this.VideoTimeLabel.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.VideoTimeLabel.ForeColor = System.Drawing.Color.Silver;
            this.VideoTimeLabel.Location = new System.Drawing.Point(199, 0);
            this.VideoTimeLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.VideoTimeLabel.Name = "VideoTimeLabel";
            this.VideoTimeLabel.Size = new System.Drawing.Size(149, 35);
            this.VideoTimeLabel.TabIndex = 3;
            this.VideoTimeLabel.Text = "label";
            this.VideoTimeLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // VolumeTrackBar
            // 
            this.VolumeTrackBar.BackColor = System.Drawing.Color.Transparent;
            colorPack4.Border = System.Drawing.Color.Red;
            colorPack4.Face = System.Drawing.Color.Red;
            colorPack4.Highlight = System.Drawing.Color.Red;
            this.VolumeTrackBar.ColorDown = colorPack4;
            colorPack5.Border = System.Drawing.Color.Red;
            colorPack5.Face = System.Drawing.Color.Red;
            colorPack5.Highlight = System.Drawing.Color.Red;
            this.VolumeTrackBar.ColorHover = colorPack5;
            colorPack6.Border = System.Drawing.Color.Red;
            colorPack6.Face = System.Drawing.Color.Red;
            colorPack6.Highlight = System.Drawing.Color.Red;
            this.VolumeTrackBar.ColorUp = colorPack6;
            this.VolumeTrackBar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.VolumeTrackBar.Dock = System.Windows.Forms.DockStyle.Left;
            this.VolumeTrackBar.FloatValueFontColor = System.Drawing.Color.Silver;
            this.VolumeTrackBar.JumpToMouse = true;
            this.VolumeTrackBar.Label = null;
            this.VolumeTrackBar.Location = new System.Drawing.Point(82, 0);
            this.VolumeTrackBar.Margin = new System.Windows.Forms.Padding(0);
            this.VolumeTrackBar.MaxValue = 100;
            this.VolumeTrackBar.Name = "VolumeTrackBar";
            this.VolumeTrackBar.ShowFocus = false;
            this.VolumeTrackBar.Size = new System.Drawing.Size(117, 35);
            this.VolumeTrackBar.SliderSize = new System.Drawing.Size(11, 11);
            this.VolumeTrackBar.SliderWidthHigh = 3F;
            this.VolumeTrackBar.SliderWidthLow = 3F;
            this.VolumeTrackBar.TabIndex = 4;
            this.VolumeTrackBar.TabStop = false;
            this.VolumeTrackBar.TickThickness = 1F;
            this.VolumeTrackBar.UpDownShow = false;
            this.VolumeTrackBar.Value = 0;
            this.VolumeTrackBar.ValueAdjusted = 0F;
            this.VolumeTrackBar.ValueBoxFontColor = System.Drawing.Color.Silver;
            this.VolumeTrackBar.ValueDivisor = gTrackBar.gTrackBar.eValueDivisor.e1;
            this.VolumeTrackBar.ValueStrFormat = null;
            this.VolumeTrackBar.ValueChanged += new gTrackBar.gTrackBar.ValueChangedEventHandler(this.OnVolumeTrackBarValueChanged);
            // 
            // VolumeButton
            // 
            this.VolumeButton.BackColor = System.Drawing.Color.Transparent;
            this.VolumeButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.VolumeButton.Dock = System.Windows.Forms.DockStyle.Left;
            this.VolumeButton.Font = new System.Drawing.Font("Segoe MDL2 Assets", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.VolumeButton.ForeColor = System.Drawing.Color.Silver;
            this.VolumeButton.Location = new System.Drawing.Point(41, 0);
            this.VolumeButton.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.VolumeButton.Name = "VolumeButton";
            this.VolumeButton.Size = new System.Drawing.Size(41, 35);
            this.VolumeButton.TabIndex = 1;
            this.VolumeButton.Text = "";
            this.VolumeButton.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.VolumeButton.Click += new System.EventHandler(this.OnVolumeButtonClick);
            // 
            // PlayButton
            // 
            this.PlayButton.BackColor = System.Drawing.Color.Transparent;
            this.PlayButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.PlayButton.Dock = System.Windows.Forms.DockStyle.Left;
            this.PlayButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.PlayButton.Font = new System.Drawing.Font("Segoe MDL2 Assets", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.PlayButton.ForeColor = System.Drawing.Color.Silver;
            this.PlayButton.Location = new System.Drawing.Point(0, 0);
            this.PlayButton.Margin = new System.Windows.Forms.Padding(0);
            this.PlayButton.Name = "PlayButton";
            this.PlayButton.Size = new System.Drawing.Size(41, 35);
            this.PlayButton.TabIndex = 0;
            this.PlayButton.Text = "";
            this.PlayButton.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.PlayButton.Click += new System.EventHandler(this.OnPlayButtonClick);
            // 
            // OverlayControl
            // 
            this.OverlayControl.BackColor = System.Drawing.Color.Transparent;
            this.OverlayControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.OverlayControl.Location = new System.Drawing.Point(0, 0);
            this.OverlayControl.Margin = new System.Windows.Forms.Padding(0);
            this.OverlayControl.Name = "OverlayControl";
            this.OverlayControl.Size = new System.Drawing.Size(740, 345);
            this.OverlayControl.TabIndex = 1;
            this.OverlayControl.MouseEnter += new System.EventHandler(this.OnPlayerMouseEnter);
            this.OverlayControl.MouseLeave += new System.EventHandler(this.OnPlayerMouseLeave);
            this.OverlayControl.MouseMove += new System.Windows.Forms.MouseEventHandler(this.OnPlayerMouseMove);
            // 
            // VideoView
            // 
            this.VideoView.BackColor = System.Drawing.Color.Black;
            this.VideoView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.VideoView.Location = new System.Drawing.Point(0, 0);
            this.VideoView.Margin = new System.Windows.Forms.Padding(0);
            this.VideoView.MediaPlayer = null;
            this.VideoView.Name = "VideoView";
            this.VideoView.Size = new System.Drawing.Size(740, 345);
            this.VideoView.TabIndex = 0;
            // 
            // NotifyIcon
            // 
            this.NotifyIcon.BalloonTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            this.NotifyIcon.BalloonTipTitle = "TorrentCat";
            this.NotifyIcon.ContextMenuStrip = this.SysTrayMenu;
            this.NotifyIcon.Icon = ((System.Drawing.Icon)(resources.GetObject("NotifyIcon.Icon")));
            this.NotifyIcon.Text = "TorrentCat";
            this.NotifyIcon.Visible = true;
            this.NotifyIcon.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.OnNotifyIconDoubleClick);
            // 
            // SysTrayMenu
            // 
            this.SysTrayMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.OpenWindowMenuItem,
            this.CloseWindowMenuItem,
            this.toolStripSeparator1,
            this.ActiveDownloadsMenuItem,
            this.toolStripSeparator2,
            this.AuthorizationMenuItem,
            this.toolStripSeparator3,
            this.ExitMenuItem});
            this.SysTrayMenu.Name = "contextMenuStrip1";
            this.SysTrayMenu.Size = new System.Drawing.Size(208, 132);
            // 
            // OpenWindowMenuItem
            // 
            this.OpenWindowMenuItem.Name = "OpenWindowMenuItem";
            this.OpenWindowMenuItem.Size = new System.Drawing.Size(207, 22);
            this.OpenWindowMenuItem.Text = "Открыть окно";
            this.OpenWindowMenuItem.Click += new System.EventHandler(this.OnNotifyIconOpenWindow);
            // 
            // CloseWindowMenuItem
            // 
            this.CloseWindowMenuItem.Name = "CloseWindowMenuItem";
            this.CloseWindowMenuItem.Size = new System.Drawing.Size(207, 22);
            this.CloseWindowMenuItem.Text = "Закрыть окно";
            this.CloseWindowMenuItem.Click += new System.EventHandler(this.OnNotifyIconCloseWindow);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(204, 6);
            // 
            // ActiveDownloadsMenuItem
            // 
            this.ActiveDownloadsMenuItem.Name = "ActiveDownloadsMenuItem";
            this.ActiveDownloadsMenuItem.Size = new System.Drawing.Size(207, 22);
            this.ActiveDownloadsMenuItem.Text = "Активные загрузки - 0";
            this.ActiveDownloadsMenuItem.Click += new System.EventHandler(this.OnNotifyIconDownloads);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(204, 6);
            // 
            // AuthorizationMenuItem
            // 
            this.AuthorizationMenuItem.Name = "AuthorizationMenuItem";
            this.AuthorizationMenuItem.Size = new System.Drawing.Size(207, 22);
            this.AuthorizationMenuItem.Text = "Авторизация";
            this.AuthorizationMenuItem.Click += new System.EventHandler(this.OnNotifyIconAuthorize);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(204, 6);
            // 
            // ExitMenuItem
            // 
            this.ExitMenuItem.Name = "ExitMenuItem";
            this.ExitMenuItem.Size = new System.Drawing.Size(207, 22);
            this.ExitMenuItem.Text = "Выход";
            this.ExitMenuItem.Click += new System.EventHandler(this.OnNotifyIconExit);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Maroon;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.ControlBox = false;
            this.Controls.Add(this.VideoContainer);
            this.Controls.Add(this.CommandContainer);
            this.Controls.Add(this.TitleBarContainer);
            this.Controls.Add(this.MainContainer);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "MainForm";
            this.Padding = new System.Windows.Forms.Padding(5);
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.TransparencyKey = System.Drawing.Color.Maroon;
            this.CommandContainer.ResumeLayout(false);
            this.VideoContainer.ResumeLayout(false);
            this.PlayControlPanel.ResumeLayout(false);
            this.UpPanel.ResumeLayout(false);
            this.UpPanel.PerformLayout();
            this.DownPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.VideoView)).EndInit();
            this.SysTrayMenu.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel MainContainer;
        private TransparentPanel TitleBarContainer;
        private System.Windows.Forms.Panel CommandContainer;
        private System.Windows.Forms.Panel VideoContainer;
        private TransparentControl OverlayControl;
        private LibVLCSharp.WinForms.VideoView VideoView;
        private System.Windows.Forms.Panel PlayControlPanel;
        private System.Windows.Forms.Panel UpPanel;
        private System.Windows.Forms.Panel DownPanel;
        private gTrackBar.gTrackBar VideoTrackBar;
        private System.Windows.Forms.Label FullScreenButton;
        private System.Windows.Forms.Label CloseButton;
        private System.Windows.Forms.Label VideoTimeLabel;
        private gTrackBar.gTrackBar VolumeTrackBar;
        private System.Windows.Forms.Label VolumeButton;
        private System.Windows.Forms.Label PlayButton;
        private System.Windows.Forms.Label TitleBarButtonExit;
        private System.Windows.Forms.Label TitleBarButtonMaximizeRestore;
        private System.Windows.Forms.Label TitleBarButtonMinimize;
        private System.Windows.Forms.NotifyIcon NotifyIcon;
        private System.Windows.Forms.ContextMenuStrip SysTrayMenu;
        private System.Windows.Forms.ToolStripMenuItem OpenWindowMenuItem;
        private System.Windows.Forms.ToolStripMenuItem CloseWindowMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem ActiveDownloadsMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem AuthorizationMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem ExitMenuItem;
        private System.Windows.Forms.Label FullWindowButton;
    }
}

