using ApiServer.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Win32;
using System;
using System.Linq;

namespace ApiServer
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services
                .AddCors()
                .AddControllers()
                .AddJsonOptions(opts => opts.JsonSerializerOptions.PropertyNamingPolicy = null);
            services.AddMemoryCache();
            services.AddMvcCore()
                .AddApiExplorer();
            services.AddSignalR();
            services.AddSingleton<Downloader>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IHostApplicationLifetime appLifetime)
        {
            appLifetime.ApplicationStarted.Register(OnStarted);
            appLifetime.ApplicationStopping.Register(OnStopping);
            appLifetime.ApplicationStopped.Register(OnStopped);

            Console.CancelKeyPress += (sender, eventArgs) =>
            {
                appLifetime.StopApplication();
                // Don't terminate the process immediately, wait for the Main thread to exit gracefully.
                eventArgs.Cancel = true;
            };

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseDefaultFiles();

            app.UseStaticFiles();

            app.UseRouting();

            app.UseCors(builder =>
                builder
                    .WithOrigins(
                        "http://localhost:8181",
                        "http://127.0.0.1:8181",
                        "http://localhost:8080",
                        "http://127.0.0.1:8080"
                        )
                    .AllowAnyMethod()
                    .AllowAnyHeader()
                    .AllowCredentials()
            );

            app.UseAuthorization();

            app.UseAuthentication();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");

                endpoints.MapFallbackToFile("/index.html");

                endpoints.MapHub<ApiHub>("/ApiHub");
            });
        }

        private void OnStarted()
        {
            var key = Registry.CurrentUser.OpenSubKey(@"SOFTWARE\TorrentCat", true);
            if (key == null)
                key = Registry.CurrentUser.CreateSubKey(@"SOFTWARE\TorrentCat", true);
            if (!key.GetValueNames().Contains("GUID"))
                key.SetValue("GUID", Guid.NewGuid().ToString().ToUpper());
        }

        private void OnStopping()
        {
            // Perform on-stopping activities here
        }

        private async void OnStopped()
        {
            await Program.host.Services.GetRequiredService<Downloader>().Shutdown();
        }
    }
}
