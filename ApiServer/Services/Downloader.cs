﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MonoTorrent.Client;
using MonoTorrent;
using System.IO;
using System.Web;
using ApiServer.Models;
using Newtonsoft.Json;
using MonoTorrent.BEncoding;
using System.Diagnostics;
using Microsoft.AspNetCore.SignalR;
using MonoTorrent.Client.PiecePicking;

namespace ApiServer.Services
{
    public class DownloadItem
    {
        public TorrentItem Item { get; set; }
        public TorrentManager Manager { get; set; }
    }

    public class Downloader
    {
        private readonly ILogger<Downloader> _logger;
        private readonly IHubContext<ApiHub> _hub;
        private readonly ClientEngine _engine = null;
        public readonly string basePath = "";
        public readonly string torrentsPath = "";
        public readonly string downloadsPath = "";
        private readonly string _fastResumeFile = "";
        private bool _shootdown = false;
        private Dictionary<int, DownloadItem> _torrents = new Dictionary<int, DownloadItem>();
        private readonly object _torrentsLock = new object();
        private BEncodedDictionary _fastResume = new BEncodedDictionary();

        public Downloader(ILogger<Downloader> logger, IHubContext<ApiHub> hub)
        {
            _logger = logger;
            _hub = hub;
            basePath = $"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData).Replace(@"\", "/")}/TorrentCat";
            torrentsPath = $"{basePath}/Torrents";
            downloadsPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.UserProfile), "Downloads")
                .Replace(@"\", "/");
            _fastResumeFile = $"{basePath}/fastresume.data";


            if (!Directory.Exists(torrentsPath))
                Directory.CreateDirectory(torrentsPath);

            if (!Directory.Exists(downloadsPath))
                Directory.CreateDirectory(downloadsPath);

            var settings = new EngineSettings
            {
                // If both encrypted and unencrypted connections are supported, an encrypted connection will be attempted
                // first if this is true. Otherwise an unencrypted connection will be attempted first.
                PreferEncryption = true,
                // Torrents will be downloaded here by default when they are registered with the engine
                SavePath = downloadsPath
            };

            _engine = new ClientEngine(settings);

            try
            {
                if (File.Exists(_fastResumeFile))
                    _fastResume = BEncodedValue.Decode<BEncodedDictionary>(File.ReadAllBytes(_fastResumeFile));
            }
            catch (Exception)
            {
                _logger.LogError("Failed to create fast resume index file.");
            }

            // Set exit and exceptions handlers
            //Console.CancelKeyPress += async (s, e) => { await Shutdown(); };

            //AppDomain.CurrentDomain.ProcessExit += async (s, e) =>
            //{
            //    _logger.LogInformation("Wait while downloader terminated...");
            //    await Shutdown();
            //};
            //AppDomain.CurrentDomain.UnhandledException += async (s, e) =>
            //{
            //    _logger.LogCritical($"Unhandled exception: {e.ExceptionObject}");
            //    await Shutdown();
            //};
            //Thread.GetDomain().UnhandledException += async (s, e) =>
            //{
            //    _logger.LogCritical($"Thread unhandled exception: {e.ExceptionObject}");
            //    await Shutdown();
            //};
        }

        public async Task CheckTorrents()
        {
            List<TorrentItem> tors = new List<TorrentItem>();
            var tbase = Path.Combine(basePath, "torrents.json");


            if (File.Exists(tbase))
            {
                using (StreamReader file = File.OpenText(tbase))
                {
                    JsonSerializer serializer = new JsonSerializer();
                    tors = (List<TorrentItem>)serializer.Deserialize(file, typeof(List<TorrentItem>));
                }

                if (tors != null)
                {
                    foreach (var task in tors)
                    {
                        if (task.Status == "downloading")
                            await StartDownloadAsync(task).ConfigureAwait(false);
                        else
                            _torrents.Add(task.ID, new DownloadItem { Item = task, Manager = null });
                    }
                }
            }
        }

        public IEnumerable<TorrentItem> GetTorrents()
        {
            return _torrents.Select(x => x.Value.Item);
        }

        public async Task<TorrentItem> StartDownloadAsync(TorrentItem item)
        {
            TorrentManager manager = null;
            TorrentItem torrentItem = item;

            if (!item.File.Contains(".torrent"))
                return torrentItem;

            if (_torrents.ContainsKey(item.ID))
            {
                if (_torrents[item.ID].Item.Status != "stopped")
                    return torrentItem;
                if (_torrents[item.ID].Manager != null)
                    manager = _torrents[item.ID].Manager;
                else
                {
                    // create new manager for restored item with nullable record
                    (TorrentItem ti, TorrentManager tm) = AddNewTorrent(item);
                    if (ti == null || tm == null)
                        return new TorrentItem();
                    lock (_torrentsLock)
                    {
                        _torrents[item.ID].Manager = tm;
                    }
                    torrentItem = ti;
                }
            }
            else
            {
                // create new torrent
                (TorrentItem ti, TorrentManager tm) = AddNewTorrent(item);
                if (ti == null || tm == null)
                    return new TorrentItem();
                lock (_torrentsLock)
                {
                    _torrents.Add(ti.ID, new DownloadItem
                    {
                        Item = ti,
                        Manager = tm
                    });
                }
                torrentItem = ti;
                manager = tm;
            }

            await _engine.Register(manager);

            // Every time a piece is hashed, this is fired.
            manager.PieceHashed += PieceHashHandler;

            // Every time the state changes (Stopped -> Seeding -> Downloading -> Hashing) this is fired
            manager.TorrentStateChanged += TorrentStateChangedHandler;

            await manager.StartAsync().ConfigureAwait(false);

            _logger.LogInformation($"Torrent with id = {item.ID} started.");

            return torrentItem;
        }

        private (TorrentItem, TorrentManager) AddNewTorrent(TorrentItem item)
        {
            Torrent torrent = null;

            var categoryPath = downloadsPath;
            if (!string.IsNullOrWhiteSpace(item.Movie.Type))
            {
                categoryPath = $"{downloadsPath}/{GetMovieType(item.Movie.Type)}";
                if (!Directory.Exists(categoryPath))
                    Directory.CreateDirectory(categoryPath);
            }

            var parts = HttpUtility.UrlDecode(item.File.Replace(@"\", "/")).Split("/");
            var tfname = parts.Where(s => s.Contains(".torrent")).ToList()[0];

            var tpath = $"{torrentsPath}/{tfname}";
            try
            {
                torrent = Torrent.Load(new Uri(item.File.Replace(@"\", "/")), tpath);
            }
            catch (Exception e)
            {
                _logger.LogError($"Failed to load torrent file with error: {e.Message}");
                return (null, null);
            }

            item.Name = torrent.Name;

            if (item.Files.Count == 0)
            {
                foreach (var file in torrent.Files)
                {
                    var path = torrent.Files.Count() > 1 ? $"{torrent.Name}/{file.FullPath}" : file.FullPath;
                    file.Priority = Priority.Normal;
                    item.Files.Add(new Models.FileInfo
                    {
                        Path = $@"{categoryPath}/{path}",
                        Length = file.Length
                    });
                }
            }
            item.Length = torrent.Size;

            var manager = new TorrentManager(torrent, categoryPath, new TorrentSettings());

            // Disable rarest first and randomised picking - only allow priority based picking (i.e. selective downloading)
            PiecePicker picker = new StandardPicker();
            picker = new PriorityPicker(picker);
            _ = manager.ChangePickerAsync(picker);

            if (_fastResume.ContainsKey(torrent.InfoHash.ToHex()))
                manager.LoadFastResume(new FastResume((BEncodedDictionary)_fastResume[torrent.InfoHash.ToHex()]));

            return (item, manager);
        }

        public async Task<TorrentItem> StopDownloadAsync(int id)
        {
            if (!_torrents.ContainsKey(id))
                return new TorrentItem();

            await _torrents[id].Manager.StopAsync();
            var item = _torrents[id].Item;
            lock (_torrentsLock)
            {
                _torrents[id].Item.Status = GetName(_torrents[id].Manager.State).ToLower();
                UpdateJsonFile();
            }
            await _engine.Unregister(_torrents[id].Manager);

            _logger.LogInformation($"Torrent with id = {item.ID} stopped.");
            return item;
        }

        private void PieceHashHandler(object s, PieceHashedEventArgs e)
        {
            lock (_torrentsLock)
            {
                var id = _torrents.FirstOrDefault(x => x.Value.Manager == e.TorrentManager).Key;
                _torrents[id].Item.Status = e.TorrentManager.Progress != 100.0
                    ? GetName(_torrents[id].Manager.State).ToLower()
                    : "downloaded";
                _torrents[id].Item.BytesCompleted = e.TorrentManager.Monitor.DataBytesDownloaded;
                foreach (var file in e.TorrentManager.Torrent.Files)
                    _torrents[id].Item.Files.FirstOrDefault(x => x.Path.Contains(file.Path)).BytesCompleted = file.BytesDownloaded;
                UpdateJsonFile();

                if (e.TorrentManager.HashChecked)
                {
                    var fastResume = new BEncodedDictionary
                    {
                        { e.TorrentManager.Torrent.InfoHash.ToHex(), e.TorrentManager.SaveFastResume().Encode() }
                    };
                    File.WriteAllBytes(_fastResumeFile, fastResume.Encode());
                }
            }
        }

        private void TorrentStateChangedHandler(object s, TorrentStateChangedEventArgs e)
        {
            if (_shootdown) return;

            var id = _torrents.FirstOrDefault(x => x.Value.Manager == e.TorrentManager).Key;
            var torrentName = _torrents[id].Manager.Torrent.Name;

            lock (_torrentsLock)
            {
                // update BytesCompleted
                _torrents[id].Item.BytesCompleted = e.TorrentManager.Monitor.DataBytesDownloaded;
                foreach (var file in e.TorrentManager.Torrent.Files)
                    _torrents[id].Item.Files.FirstOrDefault(x => x.Path.Contains(file.Path)).BytesCompleted = file.BytesDownloaded;

                if (_torrents[id].Item.Status != "downloaded")
                {
                    // Update state
                    _torrents[id].Item.Status = GetName(e.NewState).ToLower();
                    UpdateJsonFile();
                }

                if (e.TorrentManager.Progress == 100.0 && e.NewState == TorrentState.Seeding)
                {
                    _logger.LogInformation($"Torrent with id = {_torrents[id].Item.ID} downloaded.");

                    // Dispose manager
                    _torrents[id].Manager.StopAsync();
                    _engine.Unregister(_torrents[id].Manager);
                    _torrents[id].Manager.Dispose();

                    // Update state
                    _torrents[id].Item.Status = "downloaded";
                    _torrents[id].Item.BytesCompleted = _torrents[id].Item.Length;

                    var categoryPath = downloadsPath;
                    // Rename downloaded torrent
                    if (!string.IsNullOrWhiteSpace(_torrents[id].Item.Movie.Title))
                    {
                        // Combine path from movie type
                        categoryPath = $"{downloadsPath}/{GetMovieType(_torrents[id].Item.Movie.Type)}";

                        if (_torrents[id].Item.Files.Count > 1)
                        {
                            _ = Task.Run(() => RenameFolder(
                                $"{categoryPath}/{torrentName}",
                                $"{categoryPath}/{_torrents[id].Item.Movie.Title}")
                                .ConfigureAwait(false));
                            for (int i = 0; i < _torrents[id].Item.Files.Count; i++)
                                _torrents[id].Item.Files[i].Path = 
                                    $"{categoryPath}/{_torrents[id].Item.Movie.Title}/{_torrents[id].Manager.Torrent.Files[i].Path}";
                        }
                        else if (_torrents[id].Item.Files.Count == 1)
                        {
                            var fileName = _torrents[id].Manager.Torrent.Files[0].Path;
                            var ext = _torrents[id].Manager.Torrent.Files[0].Path.Split(".").Last();
                            _ = Task.Run(() => RenameFile($"{categoryPath}/{fileName}", $"{categoryPath}/{_torrents[id].Item.Movie.Title}.{ext}"))
                                .ConfigureAwait(false);
                            _torrents[id].Item.Files[0].Path = $"{categoryPath}/{_torrents[id].Item.Movie.Title}.{ext}";
                        }
                    }
                    UpdateJsonFile();
                    _hub.Clients.Group("TorrentCat").SendAsync("Downloaded", _torrents[id].Item);
                }
            }
        }

        private void UpdateJsonFile()
        {
            using var file = File.CreateText($"{basePath}/torrents.json");
            JsonSerializer serializer = new JsonSerializer();
            serializer.Serialize(file, _torrents.Select(x => x.Value.Item).ToArray());
        }

        private string GetName(TorrentState value)
        {
            return Enum.GetName(typeof(TorrentState), value);
        }

        public async Task Shutdown()
        {
            _shootdown = true;

            BEncodedDictionary fastResume = new BEncodedDictionary();
            foreach (var item in _torrents)
            {
                var m = item.Value.Manager;
                var actualState = item.Value.Item.Status;
                if (m != null && m.State != TorrentState.Stopping)
                    await m.StopAsync();
                while (m != null && m.State != TorrentState.Stopped)
                {
                    _logger.LogInformation($"{m.Torrent.Name} is {m.State}");
                    await Task.Delay(250);
                }
                lock (_torrentsLock)
                {
                    _torrents[item.Key].Item.Status = actualState;
                    UpdateJsonFile();
                    if (m != null && m.HashChecked)
                        fastResume.Add(m.Torrent.InfoHash.ToHex(), m.SaveFastResume().Encode());
                }
            }
            
            await Task.Delay(3000);
            lock(_torrentsLock)
            {
                File.WriteAllBytes(_fastResumeFile, fastResume.Encode());
            }
            _engine.Dispose();
            _logger.LogInformation($"Torrent engine stopped.");
        }

        private string GetMovieType(string type)
        {
            string result = type;

            if (type == "movie")
                result = "Фильмы";
            if (type == "cartoon")
                result = "Мультфильмы";
            if (type == "series")
                result = "Сериалы";
            if (type == "anime")
                result = "Аниме";
            if (type == "user")
                result = "Пользовательские";

            return result;
        }

        private async Task RenameFolder(string source, string destination)
        {
            await Task.Delay(3000);
            try
            {
                Directory.Move(source, destination);
            }
            catch (IOException e)
            {
                _logger.LogError($"Failed to move directory '{source}' with error: {e.Message}");
            }
        }

        private async Task RenameFile(string source, string destination)
        {
            await Task.Delay(3000);
            try
            {
                File.Move(source, destination);
            }
            catch (IOException e)
            {
                _logger.LogError($"Failed to move file '{source}' with error: {e.Message}");
            }
        }

        public async Task OpenFolder(int id)
        {   
            var folderPath = downloadsPath;

            if (!_torrents.ContainsKey(id) || _torrents[id]?.Item.Status != "downloaded")
                return;

            folderPath = Path.GetDirectoryName(_torrents[id].Item.Files[0].Path);

            if (Directory.Exists(folderPath))
            {
                var startInfo = new ProcessStartInfo
                {
                    Arguments = folderPath,
                    FileName = "explorer.exe"
                };
                Process.Start(startInfo);
                await Task.Delay(50);
            }
        }

        public async Task RemoveTorrent(int id)
        {
            if (!_torrents.ContainsKey(id))
                return;

            if (_torrents[id].Manager != null && _fastResume.ContainsKey(_torrents[id].Manager.Torrent.InfoHash.ToHex()))
                _fastResume.Remove(_torrents[id].Manager.Torrent.InfoHash.ToHex());

            if (_torrents[id].Manager != null && _torrents[id].Item.Status != "stopped")
            {
                await _torrents[id].Manager.StopAsync();
                await _engine.Unregister(_torrents[id].Manager);
                _torrents[id].Manager.Dispose();
            }

            if (_torrents[id].Item.Files.Count > 1)
            {
                var folder = Path.GetDirectoryName(_torrents[id].Item.Files[0].Path);
                try
                {
                    Directory.Delete(folder, true);
                }
                catch (IOException e)
                {
                    _logger.LogError($"Failed to delete directory {folder} with error: {e.Message}");
                }
            }
            else
            {
                try
                {
                    File.Delete(_torrents[id].Item.Files[0].Path);
                }
                catch (IOException e)
                {
                    _logger.LogError($"Failed to delete file {_torrents[id].Item.Files[0].Path} with error: {e.Message}");
                }   
            }

            lock (_torrentsLock)
            {
                _torrents.Remove(id);
                UpdateJsonFile();
            }
        }
    }
}
