﻿using CefSharp;
using CefSharp.WinForms;

using LibVLCSharp.Shared;

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using TorrentCat.Services;

namespace TorrentCat
{
    static class Program
    {
        public static Form MainWindow;
        public static ApiSubProcess ApiProcess;
        public static bool AlreadyRunning = false;

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);


            //For Windows 7 and above, best to include relevant app.manifest entries as well
            Cef.EnableHighDPISupport();

            //We use our current exe as the BrowserSubProcess
            var exePath = Process.GetCurrentProcess().MainModule?.FileName;

            var settings = new CefSettings()
            {
                CachePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData),
                    $@"{Assembly.GetCallingAssembly().GetName().Name}\CefSharp\Cache"),
                LogFile = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData),
                    $@"{Assembly.GetCallingAssembly().GetName().Name}\Chromium.log"),
                LogSeverity = LogSeverity.Default,
                BrowserSubprocessPath = exePath,
#if DEBUG
                RemoteDebuggingPort = 9696
#endif
            };

            //Example of setting a command line argument
            //Enables WebRTC
            settings.CefCommandLineArgs.Add("enable-media-stream");

            //Perform dependency check to make sure all relevant resources are in our output directory.
            Cef.Initialize(settings, performDependencyCheck: true, browserProcessHandler: null);


            // Run or connect to running API service
            ApiProcess = new ApiSubProcess();
            ApiProcess.Run();

            // Create form
            MainWindow = new MainForm();
            if (AlreadyRunning)
            {
                Cef.Shutdown();
                return;
            }
            MainWindow.ShowInTaskbar = false;
            MainWindow.WindowState = FormWindowState.Minimized;
            Application.Run(MainWindow);
        }
    }
}
